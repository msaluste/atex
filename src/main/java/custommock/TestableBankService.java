package custommock;

import common.BankService;
import common.Money;

public class TestableBankService implements BankService {

    public boolean wasWithdrawCalledWith(Money money, String account) {

        // siin peaks võrdlema praeguseid argumente meeldejäetud argumentidega

        throw new IllegalStateException("not implemented");
    }

    public boolean wasDepositCalledWith(Money money, String account) {

        // siin peaks võrdlema praeguseid argumente meeldejäetud argumentidega

        throw new IllegalStateException("not implemented");
    }

    @Override
    public void withdraw(Money money, String fromAccount) {
        System.out.println("credit: " + money + " - " + fromAccount);

        // siin peaks argumendid (money ja fromAccount) meelde jätma

    }

    @Override
    public void deposit(Money money, String toAccount) {
        System.out.println("debit: " + money + " - " + toAccount);
        // siin peaks argumendid (money ja toAccount) meelde jätma

    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return true;
    }

    public void setSufficentFundsAvailable(boolean areFundsAvailable) {
        // hasSufficientFundsFor() tagastab praegu alati true.
        // See meetod peaks määrama, kuidas hasSufficientFundsFor() vastab.
    }

    public boolean wasWithdrawCalled() {
        // Meetod peaks ütema, kas meetodit credit() välja kutsuti
        // (mistahes argumentidega)

        throw new IllegalStateException("not implemented");
    }

}