package invoice;

import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.text.*;
import java.util.Date;

import org.hamcrest.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceRowGeneratorTest {

    @Mock
    InvoiceRowDao dao;

    @InjectMocks
    InvoiceRowGenerator generator;

    @Test
    public void dividesEquallyWhenNoRemainder() {

        generator.generateRowsFor(new BigDecimal(9),
                                  asDate("2012-02-15"),
                                  asDate("2012-04-02"));

        verify(dao, times(3)).save(argThat(getMatcherForAmount(3)));

        // verify that there are no more calls
    }

    private Matcher<InvoiceRow> getMatcherForAmount(final Integer amount) {

        // Matcher-i näide. Sama põhimõttega tuleb teha teine
        // Kuupäeva jaoks

        return new TypeSafeMatcher<InvoiceRow>() {

            @Override
            protected boolean matchesSafely(InvoiceRow item) {
                return amount.equals(item.amount.intValue());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.valueOf(amount));
            }
        };
    }

    public static Date asDate(String date) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}