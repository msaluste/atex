package selenium;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import selenium.pages.LoginPage;

public class PageObjectTest {

    private static final String USERNAME = "user";
    private static final String CORRECT_PASSWORD = "1";
    private static final String WRONG_PASSWORD = "2";

    @Test
    public void loginFailsWithFalseCredentials() {
        LoginPage page = LoginPage.goTo();

        page = page.logInWithExpectingFailure(USERNAME, WRONG_PASSWORD);

        assertThat(page.getPageId(), is(LoginPage.ID));
        assertThat(page.getErrorMessage(), is(notNullValue()));
    }

}
