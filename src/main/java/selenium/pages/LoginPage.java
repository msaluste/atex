package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends AbstractPage {

    public static final String ID = "login_page";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage logInWithExpectingFailure(String user, String pass) {

        // sisestage kasutajanimi
        // sisestage salasõna
        // vajutage sisse logimise nupule

        if (getErrorMessage() != null) {
            // kui oli viga, siis tagastage sisselogimise leht
        }

        throw new IllegalStateException("no error message");
    }

    public String getErrorMessage() {
        WebElement element = elementById("error_message");
        return element == null ? null : element.getText();
    }

    public static LoginPage goTo() {
        WebDriver driver = getDriver();
        driver.get(BASE_URL);
        return new LoginPage(driver);
    }

}
