package selenium.pages;

import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public abstract class AbstractPage {

    public static final String BASE_URL = "http://enos.itcollege.ee/~mkalmo/selenium";

    protected final WebDriver driver;

    public AbstractPage(WebDriver driver) {
        this.driver = driver;
    }

    protected static WebDriver getDriver() {
        return new HtmlUnitDriver();
    }

    protected WebElement elementById(String id) {
        List<WebElement> elements = driver.findElements(By.id(id));
        return elements.isEmpty() ? null : elements.get(0);
    }

    public String getPageId() {
        List<WebElement> elements = driver.findElements(By.tagName("body"));
        return elements.isEmpty() ? null : elements.get(0).getAttribute("id");
    }

}
